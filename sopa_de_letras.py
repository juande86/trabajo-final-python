import os, json
import PySimpleGUI as sg
from pathlib import Path
from utiles import funciones
from utiles import exportador as exp
from utiles.clases import grilla

recursos = os.getcwd() + os.sep + 'recursos' + os.sep

sg.SetOptions(icon=Path(str(recursos + 'notebook.ico')))   # POR AHORA NO FUNCIONA EL CAMBIO DE INCONO EN LINUX

def splash():
    """
    Función que ejecuta el juego, equivalente a un main
    """

    layout_menu = [
        ['Inicio', ['Nuevo Juego', 'Salir']],
        ['Ajustes', ['Configurar', 'Reporte']],
        ['Ayuda', ['Acerca de Sopa de Letras']]
    ]

    layout = [
        [sg.Menu(layout_menu)],
        [sg.Image(filename=recursos + 'splash.png')]
    ]

    ventana = sg.Window('Sopa de Letras!').Layout(layout)
    configurado = False
    while True:
        boton, eventos = ventana.Read()
        if boton is 'Salir' or boton is None:
            break
        elif boton is 'Nuevo Juego':
            if not configurado:
                configurado = True
                config = funciones.configurar()
            tablero = grilla(config)
            try:
                archivo = open(recursos + 'palabras.json', 'r')
                palabras = json.load(archivo)
            except:
                sg.Popup('No hay palabras cargadas')
                exp.agregar_palabras()
                archivo = open(recursos + 'palabras.json', 'r')
                palabras = json.load(archivo)
            tablero.palabras_random(palabras)
            tablero.crear_grillas()
            funciones.mostrar_grilla(tablero)
        elif boton is 'Configurar':
            configurado = True
            config = funciones.configurar()
        elif boton is 'Reporte':
            if configurado:
                exp.mostrar_reporte(config['FUENTE_TIT'], int(config['TAM_TIT']), config['FUENTE_PAR'], int(config['TAM_PAR']))
            else:
                exp.mostrar_reporte('Arial', 15, 'Arial', 10)
        else:
            sg.Popup('Sopa de Letras! v0.1\n'
                     'Creada por\n'
                     '\tManuel Pastor\n'
                     '\tJuan de Dios Lettieri\n'
                     'Para la Cátedra de Seminario de Lenguajes - Python'
                     '\nUNLP - Año 2019', title='Acerca de Sopa de Letras!')

    ventana.Close()

if __name__ == '__main__':
    splash()

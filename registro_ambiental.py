import time
import os
import json
import PySimpleGUI as sg
from utiles.temperatura import Temperatura


def principal ():

    try:
        archivo = os.getcwd() + os.sep + 'recursos' + os.sep + 'datos-oficinas.json'
        with open(archivo, 'r') as arch:
            registro = json.load(arch)
        arch.close()
    except:
        nombre = sg.PopupGetText('Ingrese nombre de la oficina a crear donde se guardarán los datos', title='Crear Archivo')
        registro = {nombre: []}

    layout = [
        [sg.Text('Seleccione oficina:')],
        [sg.Combo(list(registro.keys()), size=(None, 3))],
        [sg.OK('Aceptar'), sg.Cancel('Cancelar'), sg.Button('Agregar')]
    ]

    ventana = sg.Window('Oficinas').Layout(layout)
    boton, oficina = ventana.Read()

    if boton is 'Cancelar' or boton is None:
        return None
    elif boton is 'Agregar':
        nombre = sg.PopupGetText('Ingrese el nombre de la oficina',title='Agregar')
        registro[nombre] = []
        tomar_registros(registro, nombre)
    else:
        tomar_registros(registro, oficina[0])

def tomar_registros(registro=dict, oficina=str):
    headings = ['Fecha', 'Temperatura', 'Humedad']
    data = []
    if len(registro[oficina])!=0:
        for x in registro[oficina]:
            data.append([x['Fecha'],x['Temp'],x['Humedad']])
    else:
        data = [['','','']]



    layout = [[sg.Table(values=data, headings=headings, justification='center', vertical_scroll_only=False,
                        num_rows=5, alternating_row_color='lightgray', def_col_width=10, auto_size_columns=True,
                        key='tabla')],
              [sg.ReadButton('Tomar Registros', key='REG'), sg.Button('Guardar')]
              ]
    ventana = sg.Window('Registros Ambientales').Layout(layout)
    pausa = True
    temp = Temperatura()
    while True:
        if pausa:
            boton, eventos = ventana.Read()
        else:
            boton, eventos = ventana.Read(timeout=0)
        if boton == 'REG':
            boton = ventana.FindElement('REG').GetText()
        if eventos is None:
            break
        elif boton is 'Tomar Registros':
            pausa = False
            element = ventana.FindElement('REG')
            element.Update(text='Parar')
            agregar_registro(registro,oficina,temp,data)
            ventana.FindElement('tabla').Update(data)
            actual = time.time()
        if boton is 'Parar':
            pausa = True
            element = ventana.FindElement('REG')
            element.Update(text='Tomar Registros')
        if not pausa:
            aux = time.time()
            if int(aux - actual) >= 10:
                agregar_registro(registro, oficina, temp,data)
                ventana.FindElement('tabla').Update(data)
                actual = time.time()

        elif boton is 'Guardar':
            archivo = '.' + os.sep + 'recursos' + os.sep + 'datos-oficinas.json'
            with open(archivo, 'w') as arch:
                arch.write(json.dumps(registro, indent=4))
            arch.close()
        time.sleep(0.1)
    return None

def agregar_registro(registro=dict, oficina=str, temp=Temperatura, data=list):
    lectura = temp.datos_sensor()
    fecha = time.strftime("%c")
    registro[oficina].append({'Fecha': fecha, 'Temp': lectura['temperatura'], 'Humedad': lectura['humedad']})
    data.append([fecha,lectura['temperatura'],lectura['humedad']])
    if data[0][0]=='':
        data.pop(0)
    return registro

if __name__ == '__main__':
    principal()
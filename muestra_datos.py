from utiles.temperatura import Temperatura
from utiles.sonido import Sonido
from utiles.matriz import Matriz
import time

matriz = Matriz(numero_matrices=2, ancho=16)
sonido = Sonido()
temperatura = Temperatura()

def mostrar_lectura():
    lectura = temperatura.datos_sensor()
    mensaje = 'Temperatura = {0:0.1f}°C,  Humedad = {1:0.1f}%'.format(lectura['temperatura'], lectura['humedad'])
    matriz.mostrar_mensaje(mensaje)

while True:
    time.sleep(0.1)
    sonido.evento_detectado(mostrar_lectura())
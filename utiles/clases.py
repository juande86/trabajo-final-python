import random
import string


class grilla:

    def __init__(self, config):
        self.__color_sust = config['COLOR_S'] if config['COLOR_S'] not in ('', None) else 'red'
        self.__color_adj = config['COLOR_A'] if config['COLOR_A'] not in ('', None) else 'blue'
        self.__color_verbo = config['COLOR_V'] if config['COLOR_V'] not in ('', None) else 'green'
        self.__cant_sust = int(config['CANT_S']) if config['CANT_S'] is not None else 3
        self.__cant_adj = int(config['CANT_A']) if config['CANT_A'] is not None else 3
        self.__cant_verbos = int(config['CANT_V']) if config['CANT_V'] is not None else 3
        self.__horizontal = config['HORIZONTAL'] if config['HORIZONTAL'] is not None else True
        self.__vertical = config['VERTICAL'] if config['VERTICAL'] is not None else False
        self.__ayuda = config['AYUDA_ON'] if config['AYUDA_ON'] is not None else False
        self.__ayuda_palabras = config['AYUDA_PAL'] if config['AYUDA_PAL'] is not None else False
        self.__ayuda_def = config['AYUDA_DEF'] if config['AYUDA_DEF'] is not None else False
        self.__mayusculas = config['MAYUSC'] if config['MAYUSC'] is not None else True
        self.__minusculas = config['MINUSC'] if config['MINUSC'] is not None else False
        self.__sustantivos = []
        self.__verbos = []
        self.__adjetivos = []
        self.__palabras = []
        self.__definiciones = []
        self.__alto = 0
        self.__ancho = 0
        self.__g_aux = None
        self.__g_palabras = None

    @property
    def grilla(self):
        return self.__g_palabras

    @property
    def solucion(self):
        return self.__g_aux

    @property
    def ancho(self):
        return self.__ancho

    @property
    def alto(self):
        return self.__alto

    @property
    def colores(self):
        return self.__color_sust, self.__color_adj, self.__color_verbo

    @property
    def cantidades(self):
        return self.__cant_sust, self.__cant_adj, self.__cant_verbos

    @property
    def palabras(self):
        retorno = [palabra.upper() for palabra in self.__palabras] if \
            self.__mayusculas else [palabra.lower() for palabra in self.__palabras]
        random.shuffle(retorno)
        return retorno

    @property
    def pistas(self):
        return self.__ayuda, self.__ayuda_palabras, self.__ayuda_def

    @property
    def definiciones(self):
        random.shuffle(self.__definiciones)
        return self.__definiciones

    def __eq__(self, grilla_usuario):
        return self.__g_aux == grilla_usuario

    def palabras_random(self, palabras=dict):
        '''Método que determina las palabras que van a formar parte de la grilla.
        También determina el ancho y largo de la grilla'''

        sustantivos = list(palabras['sustantivos'].keys())
        if self.__cant_sust >= len(sustantivos):
            self.__sustantivos = sustantivos
            self.__cant_sust = len(sustantivos)
        else:
            self.__sustantivos = random.sample(sustantivos, k=self.__cant_sust)
        for palabra in self.__sustantivos:
            self.__definiciones.append(palabras['sustantivos'][palabra])

        verbos = list(palabras['verbos'].keys())
        if self.__cant_verbos >= len(verbos):
            self.__verbos = verbos
            self.__cant_verbos = len(verbos)
        else:
            self.__verbos = random.sample(verbos, k=self.__cant_verbos)
        for palabra in self.__verbos:
            self.__definiciones.append(palabras['verbos'][palabra])

        adjetivos = list(palabras['adjetivos'].keys())
        if self.__cant_adj >= len(adjetivos):
            self.__adjetivos = adjetivos
            self.__cant_adj = len(adjetivos)
        else:
            self.__adjetivos = random.sample(adjetivos, k=self.__cant_adj)
        for palabra in self.__adjetivos:
            self.__definiciones.append(palabras['adjetivos'][palabra])

        self.__palabras = self.__sustantivos.copy() + self.__adjetivos.copy() + self.__verbos.copy()
        maximo = len(max(self.__palabras, key=lambda x: len(x)))
        if len(self.__palabras) > maximo:
            self.__ancho = len(self.__palabras) + 2
            self.__alto = len(self.__palabras) + 2
        else:
            self.__ancho = maximo + 2
            self.__alto = maximo + 2

    def crear_grillas(self):

        self.__g_palabras = [[random.choice(string.ascii_uppercase) for col in range(self.__ancho)] for fila in
                             range(self.__alto)] if self.__mayusculas else [
            [random.choice(string.ascii_lowercase) for col in range(self.__ancho)] for fila in range(self.__alto)]

        self.__g_aux = [[0 for col in range(self.__ancho)] for fila in range(self.__alto)]

        palabras = self.__palabras.copy()

        if self.__horizontal:
            libres = [x for x in range(self.__alto)]
            while len(palabras) > 0:
                palabra = palabras.pop(random.choice(range(len(palabras))))
                fila = libres.pop(random.choice(range(len(libres))))
                col = random.choice(range(self.__ancho - len(palabra) + 1))
                if palabra in self.__sustantivos:
                    aux = 1
                elif palabra in self.__adjetivos:
                    aux = 2
                else:
                    aux = 3
                palabra = palabra.upper() if self.__mayusculas else palabra.lower()
                for letra in palabra:
                    self.__g_aux[fila][col] = aux
                    self.__g_palabras[fila][col] = letra
                    col += 1
        else:
            libres = [x for x in range(self.__ancho)]
            while len(palabras) > 0:
                palabra = palabras.pop(random.choice(range(len(palabras))))
                col = libres.pop(random.choice(range(len(libres))))
                fila = random.choice(range(self.__alto - len(palabra) + 1))
                if palabra in self.__sustantivos:
                    aux = 1
                elif palabra in self.__adjetivos:
                    aux = 2
                else:
                    aux = 3
                palabra = palabra.upper() if self.__mayusculas else palabra.lower()
                for letra in palabra:
                    self.__g_aux[fila][col] = aux
                    self.__g_palabras[fila][col] = letra
                    fila += 1
import PySimpleGUI as sg
import utiles.exportador as exp
from utiles.clases import grilla
from tkinter import font
import tkinter
import os
import json
import random
from playsound import playsound

recursos = os.getcwd() + os.sep + 'recursos' + os.sep

def configurar():
    """
    Función que muestra una ventana con distintos parámetros a configurar de la grilla y las palabras que la van a
    componer, devuelve un diccionario con toda la información etiquetada
    """
    root = tkinter.Tk()
    lista_fuentes = list(font.families())
    lista_fuentes.sort()
    root.destroy()

    try:
        with open(os.getcwd() + os.sep + 'recursos' + os.sep + 'datos-oficinas.json', 'r') as arch:
            oficinas = json.load(arch)
        arch.close()
    except:
        oficinas = {'Oficina1': [{'Fecha': None, 'Temperatura': None, 'Humedad': None}]}

    layout_palabras = [
        [sg.Button('Agregar', size=(8, 1))],
        [sg.Button('Eliminar', size=(8, 1))],
        [sg.Button('Mostrar', size=(8, 1))]
    ]

    layout_colores = [
        [sg.Text('Sustantivos', size=(11, 1)), sg.ColorChooserButton('Elegir Color', key='COLOR_S')],
        [sg.Text('Verbos', size=(11, 1)), sg.ColorChooserButton('Elegir Color', key='COLOR_V')],
        [sg.Text('Adjetivos', size=(11, 1)), sg.ColorChooserButton('Elegir Color', key='COLOR_A')]
    ]

    layout_ayuda = [
        [sg.Radio('Sin Ayuda', group_id='AYUDA', change_submits=True, default=True, key='AYUDA_OFF'),
         sg.Radio('Con Ayuda', group_id='AYUDA', change_submits=True, key='AYUDA_ON')],
        [sg.Checkbox('Mostrar Definiciones', disabled=True, key='AYUDA_DEF', default=True),
         sg.Checkbox('Mostrar Palabras', disabled=True, key='AYUDA_PAL')],
    ]

    layout_orientacion = [
        [sg.Radio('Horizontal', group_id='ORIENTACION', key='HORIZONTAL', default=True),
         sg.Radio('Vertical', group_id='ORIENTACION', key='VERTICAL')],
        [sg.Radio('Mayúsculas', group_id='TAM', key='MAYUSC', default=True),
         sg.Radio('Minúsculas', group_id='TAM', key='MINUSC')],
    ]

    layout_cantidades = [
        [sg.Spin([x for x in range(11)], initial_value=3, size=(2, 1), key='CANT_S'), sg.Text('Sustantivos')],
        [sg.Spin([x for x in range(11)], initial_value=3, size=(2, 1), key='CANT_V'), sg.Text('Verbos')],
        [sg.Spin([x for x in range(11)], initial_value=3, size=(2, 1), key='CANT_A'), sg.Text('Adjetivos')],
        [sg.Text('Máximo de palabras en total: 15')]
    ]

    layout_temas = [
        [sg.Radio('Manual', group_id='TEMA', key='TEMA_MANUAL', enable_events=True)],
        [sg.Text('Seleccione Tema'), sg.Combo(sg.ListOfLookAndFeelValues(), disabled=True, key='LISTA_TEMAS')],
        [sg.Radio('Automático', group_id='TEMA', key='TEMA_AUTOMATICO', default=True, enable_events=True)],
        [sg.Text('Seleccine Oficina'), sg.Combo(list(oficinas.keys()), key='OFICINA')]
    ]

    layout_reporte_titulo = [
        [sg.Text('Tipografía'),
         sg.InputCombo(lista_fuentes, default_value=lista_fuentes[lista_fuentes.index('Arial')] if 'Arial'
                                        in lista_fuentes else lista_fuentes[0], size=(None, 10), key='FUENTE_TIT')],
        [sg.Text('Tamaño'), sg.Slider((5, 50), orientation='h', key='TAM_TIT', default_value=15)]
    ]

    layout_reporte_parrafo = [
        [sg.Text('Tipografía'),
         sg.InputCombo(lista_fuentes,
                       default_value=lista_fuentes[lista_fuentes.index('Arial')] if 'Arial' in lista_fuentes else
                       lista_fuentes[0], size=(None, 10), key='FUENTE_PAR')],
        [sg.Text('Tamaño'), sg.Slider((5, 50), orientation='h', key='TAM_PAR', default_value=10)]
    ]

    layout_reporte = [
        [sg.Frame('Titulos', layout_reporte_titulo)],
        [sg.Frame('Párrafos', layout_reporte_parrafo)]
    ]

    layout = [
        [sg.Frame('Palabras', layout_palabras), sg.VerticalSeparator(), sg.Frame('Colores', layout_colores)],
        [sg.Frame('Ayuda', layout_ayuda)],
        [sg.Frame('Orientación', layout_orientacion), sg.VerticalSeparator(),
         sg.Frame('Cantidades', layout_cantidades)],
        [sg.Frame('Temas', layout_temas), sg.VerticalSeparator(), sg.Frame('Reporte', layout_reporte)],
        [sg.Button('Aceptar'), sg.Button('Vista Previa'), sg.Button('Cerrar')]
    ]

    ventana = sg.Window('Configuración').Layout(layout)

    while True:
        boton, eventos = ventana.Read()
        if boton is 'Aceptar':
            if (int(eventos['CANT_S']) + int(eventos['CANT_V']) + int(eventos['CANT_A']) > 15):
                sg.Popup('Se excedió el máximo de palabras permitidas, quitá algunas!', title='')
            elif (int(eventos['CANT_S']) + int(eventos['CANT_V']) + int(eventos['CANT_A']) == 0):
                sg.Popup('La cantidad de palabras debe ser mayor a 0', title='')
            else:
                look_and_feel(eventos['TEMA_AUTOMATICO'], eventos['OFICINA'], eventos['LISTA_TEMAS'])
                break
        if boton is 'Cerrar' or boton is None:
            ventana.Close()
            return eventos

        if eventos['AYUDA_ON']:
            ventana.FindElement(key='AYUDA_PAL').Update(disabled=False)
            ventana.FindElement(key='AYUDA_DEF').Update(disabled=False)
        else:
            ventana.FindElement(key='AYUDA_PAL').Update(disabled=True)
            ventana.FindElement(key='AYUDA_DEF').Update(disabled=True)

        if boton is 'TEMA_MANUAL':
            ventana.FindElement(key='LISTA_TEMAS').Update(disabled=False)
            ventana.FindElement(key='OFICINA').Update(disabled=True)
        elif boton is 'TEMA_AUTOMATICO':
            ventana.FindElement(key='LISTA_TEMAS').Update(disabled=True)
            ventana.FindElement(key='OFICINA').Update(disabled=False)

        if boton is 'Vista Previa':
            look_and_feel(eventos['TEMA_AUTOMATICO'], eventos['OFICINA'], eventos['LISTA_TEMAS'])
            if eventos['COLOR_S'] is '':
                eventos['COLOR_S'] = 'red'
            if eventos['COLOR_A'] is '':
                eventos['COLOR_A'] = 'blue'
            if eventos['COLOR_V'] is '':
                eventos['COLOR_V'] = 'green'
            vista_previa(eventos['MINUSC'], eventos['COLOR_S'], eventos['COLOR_A'], eventos['COLOR_V'])

        elif boton is 'Agregar':
            exp.agregar_palabras()
        elif boton is 'Eliminar':
            exp.eliminar_palabras()
        elif boton is 'Mostrar':
            exp.mostrar_palabras()

    ventana.Close()
    return eventos


def mostrar_grilla(tablero=grilla):
    """
    Función que recibe como parámetro un objeto tipo "grilla" del archivo clases.py
    Crea una ventana que muestra la implementación de la sopa de letras, devuelve None.
    """
    grilla_palabras = tablero.grilla
    columnas = [x for x in range(tablero.ancho)]
    filas = [x for x in range(tablero.alto)]

    layout_grilla = [
        [sg.Button(button_text=grilla_palabras[f][c],
                   size=(4, 2),
                   border_width=0,
                   enable_events=True,
                   key=str(c) + ',' + str(f),
                   button_color=('black', 'white'),
                   pad=(0, 0),
                   auto_size_button=False)
         for c in columnas] for f in filas
    ]

    layout_botones = [[sg.Radio('Sustantivos', 'colores', default=True, text_color=tablero.colores[0]),
                       sg.Radio('Adjetivos', 'colores', text_color=tablero.colores[1]),
                       sg.Radio('Verbos', 'colores', text_color=tablero.colores[2])]]

    layout_cantidades = [
        [sg.Text('Sustantivos: {}'.format(tablero.cantidades[0]))],
        [sg.Text('Adjetivos: {}'.format(tablero.cantidades[1]))],
        [sg.Text('Verbos: {}'.format(tablero.cantidades[2]))]
    ]

    layout_pista = [
        [sg.Text(pal)] for pal in tablero.palabras
    ]

    layout_costado = [
        [sg.Frame('Cantidades', layout_cantidades)],
        [sg.Button('Pista')],
        [sg.Button('Verificar')],
        [sg.Button('Cerrar')],
        [sg.Frame('Palabras', layout_pista, visible=False, key='ayuda_pal')]
    ]

    layout_definiciones = [
        [sg.Multiline(mostrar_definiciones(tablero.definiciones), disabled=True, size=(50, 25))]
    ]

    layout_total = [[sg.Frame('Palabras', layout_botones)],
                    [sg.Frame('Sopa de letras', layout_grilla), sg.VerticalSeparator(), sg.Frame('', layout_costado),
                    sg.VerticalSeparator(), sg.Frame('Definiciones', layout_definiciones, visible=False, key='DEF')]]

    layout = [
        [sg.Column(layout_total, scrollable=True, size=(1300, 1000))]
    ]

    ventana = sg.Window('Sopa de Letras', size=(750, 1000), resizable=True).Layout(layout).Finalize()

    aux = [[0 for col in columnas] for fila in filas]

    while True:
        events, values = ventana.Read()
        if events == 'Verificar':
            if tablero == aux:
                try:
                    playsound(recursos + 'happykids.wav', block=False)
                except NotImplementedError:
                    playsound(recursos + 'happykids.wav', block=True)
                sg.Popup('Felicitaciones! Completaste la Sopa de Letras!', title='')
            else:
                verificar(aux, tablero.solucion)
        elif events == 'Pista':
            if tablero.pistas[0]:
                if tablero.pistas[1]:
                    ventana.FindElement(key='ayuda_pal').Update(visible=True)
                if tablero.pistas[2]:
                    ventana.FindElement('DEF').Update(visible=True)
                    #mostrar_definiciones(tablero.definiciones)
            else:
                sg.Popup('No tenés pistas disponibles :(', title='')
        elif events == None or events is 'Cerrar':
            break
        else:
            y = events
            col = y[0:y.find(',')]
            fila = y[y.find(',') + 1:]
            if values[0] == True:
                if events != None and events != 'Verificar':
                    if aux[int(fila)][int(col)] != 1:
                        ventana.FindElement(key=y).Update(button_color=('white', tablero.colores[0]))
                        aux[int(fila)][int(col)] = 1
                    else:
                        ventana.FindElement(key=y).Update(button_color=('black', 'white'))
                        aux[int(fila)][int(col)] = 0
            elif values[1] == True:
                if events != None and events != 'Verificar':
                    if aux[int(fila)][int(col)] != 2:
                        ventana.FindElement(key=y).Update(button_color=('white', tablero.colores[1]))
                        aux[int(fila)][int(col)] = 2
                    else:
                        ventana.FindElement(key=y).Update(button_color=('black', 'white'))
                        aux[int(fila)][int(col)] = 0
            else:
                if events != None and events != 'Verificar':
                    if aux[int(fila)][int(col)] != 3:
                        ventana.FindElement(key=y).Update(button_color=('white', tablero.colores[2]))
                        aux[int(fila)][int(col)] = 3
                    else:
                        ventana.FindElement(key=y).Update(button_color=('black', 'white'))
                        aux[int(fila)][int(col)] = 0
    ventana.Close()


def mostrar_definiciones(definiciones=list):
    """
    :param definiciones: lista de strings
    :return: un solo string procesado para mostrar en pantalla
    """
    texto = ''
    for elem in definiciones:
        texto += elem + '\n{}\n'.format('-' * 80)
    return texto


def look_and_feel(automatico=bool, oficina=str, tema=str):
    """
    Actualiza el estilo de todas las ventanas de acuerdo a los parámetros que recibe.
    Si automatico=True entonces tomará datos de las oficinas que toman valores ambientales y determinará de acuerdo
    a criterios que aplicaremos en la próxima entrega si se decanta por un tema o el otro.
    Si se elige alguno de los temas de manera manual, se implementa ese tema.
    """
    if automatico:
        try:
            with open('.' + os.sep + 'recursos' + os.sep + 'datos-oficinas.json', 'r') as arch:
                datos = json.load(arch)
            arch.close()
        except:
            #   Si se genera alguna excepción manejando al intentar abrir el archivo, se elegirá un estilo al azar.
            sg.ChangeLookAndFeel(random.choice(sg.ListOfLookAndFeelValues()))
            return None
        #   Acá debería hacer un promedio de las temperaturas y humedades y decidir como va a elegir el estilo.
        temp, hum = 0.0, 1.0
        try:
            for toma in datos[oficina]:
                temp = temp + float(toma['Temp'])
                hum = hum + float(toma['Humedad'])
            temp = temp/len(datos[oficina])
            hum = hum/len(datos[oficina])
        except:
            #   Caso de tener cargada una oficina sin valores
            temp = random.choice(list(range(38)))
            hum = random.choice(list(range(1,101)))
        tema = int(hum // temp)
        try:
            sg.ChangeLookAndFeel(sg.ListOfLookAndFeelValues()[tema])
        except IndexError:
            sg.ChangeLookAndFeel(sg.ListOfLookAndFeelValues()[tema-5])
    else:
        sg.ChangeLookAndFeel(tema)


def vista_previa(minusc=bool, c_sust=str, c_adj=str, c_verb=str):
    """
    Funcion que crea una ventana a modo de "Vista Previa" con los valores que determinó el usuario
    :param minusc: booleano, True si es minúsculas, False si es mayúsculas
    :param c_sust: color sustantivos en formato hexadecimal, ej: #FFFFFF
    :param c_adj: color adjetivos en formato hexadecimal
    :param c_verb: color verbos en formato hexadecimal
    :return: None
    """
    layout_palabras = [
        [sg.Text('Sustantivos', text_color=c_sust, size=(11, 1)),
         sg.Button('A', button_color=('white', c_sust), key='SUST')],
        [sg.Text('Adjetivos', text_color=c_adj, size=(11, 1)),
         sg.Button('A', button_color=('white', c_adj), key='ADJ')],
        [sg.Text('Verbos', text_color=c_verb, size=(11, 1)), sg.Button('A', button_color=('white', c_verb), key='VERB')]
    ] if not minusc else [
        [sg.Text('Sustantivos', text_color=c_sust, size=(11, 1)),
         sg.Button('a', button_color=('white', c_sust), key='SUST')],
        [sg.Text('Adjetivos', text_color=c_adj, size=(11, 1)),
         sg.Button('a', button_color=('white', c_adj), key='ADJ')],
        [sg.Text('Verbos', text_color=c_verb, size=(11, 1)), sg.Button('a', button_color=('white', c_verb), key='VERB')]
    ]

    layout = [
        [sg.Frame('Vista Previa', layout_palabras)],
        [sg.Button('Cerrar')]
    ]

    ventana = sg.Window('Vista Previa').Layout(layout)

    while True:
        boton, eventos = ventana.Read()
        if boton is 'Cerrar' or boton is None:
            break
    ventana.Close()


def verificar(grilla=list, solucion=list):
    """
    Función que verifica campo a campo, comparando la grilla con la solucion.
    Lleva contadores para todos los casos:
        1. Casilleros que faltan marcar
        2. Casilleros mal marcados, o marcados de más
        3. Casilleros mal tipificados; ej: marcado como sustantivo en vez de adjetivo
        4. Casilleros bien marcados, sin contar los que no deben ser modificados
    :param grilla: matriz de enteros de estilo [[int]] con los valores de la grilla creada por el usuario
    :param solucion: matriz de enteros solución, se obtiene del objeto grilla en el archivo clases.py
    :return: None
    """
    falta, mal, tipo, bien = 0, 0, 0, 0
    for col in range(len(grilla)):
        for fila in range(len(grilla[col])):
            if solucion[col][fila] != grilla[col][fila]:
                if solucion[col][fila] == 0:
                    mal += 1
                elif grilla[col][fila] == 0:
                    falta += 1
                else:
                    tipo += 1
            else:
                if solucion[col][fila] != 0:
                    bien += 1
    layout = [
        [sg.Text('Revisemos la sopa!', justification='center', size=(60, 1))],
        [sg.Text('Hay {falta} casilleros que todavía no han sido descubiertos'.format(falta=falta),
                 justification='center', size=(60, 1))],
        [sg.Text('Hay {tipo} casilleros que se clasifican de otra manera'.format(tipo=tipo), justification='center',
                 size=(60, 1))],
        [sg.Text('Hay {mal} casilleros que están marcados de más'.format(mal=mal), justification='center',
                 size=(60, 1))],
        [sg.Text('Felicitaciones! Hay {bien} casilleros bien marcados!'.format(bien=bien), justification='center',
                 size=(60, 1))],
        [sg.CloseButton('Seguir Jugando')]
    ]

    ventana = sg.Window('Ups!').Layout(layout)
    boton = ventana.Read()
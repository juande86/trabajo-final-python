import json
import os
import PySimpleGUI as sg
from pattern.web import Wiktionary
from pattern.text.es import predicative, parse, verbs, spelling, lexicon


def mostrar_palabras():
    """
    Toma valores del archivo "palabras.json" y muestra una ventana con su contenido, en caso de tenerlo.
    :return: None
    """
    path_palabras = '.' + os.sep + 'recursos' + os.sep + 'palabras.json'
    try:
        with open(path_palabras, 'r') as arch:
            palabras = json.load(arch)
            arch.close()
    except:
        sg.Popup('No hay palabras cargadas para mostrar.')
        return None

    texto = ''
    for tipo in palabras.keys():
        texto += tipo.upper() + '\n\n'
        pal = list(palabras[tipo].items())
        pal.sort()
        for palabra, definicion in pal:
            texto += '{palabra}:\n\t{definicion}\n\n'.format(palabra=palabra.capitalize(), definicion=definicion)
            texto += ('-'*200) + '\n\n'
        texto += ('='*100) + '\n\n'

    layout = [
        [sg.Multiline(texto, size=(120, 30))],
        [sg.OK('Aceptar')]
    ]

    ventana = sg.Window('Palabras cargadas').Layout(layout)
    while True:
        boton, datos = ventana.Read()
        if boton is 'Aceptar' or boton is None:
            break
    ventana.Close()


def buscar_definicion(palabra=str):
    """
    Función que recibe un string, lo busca en Wiktionary y devuelve una tupla donde el primer elemento
    es el tipo de palabra (Sustantivo, Verbo, Adjetivo) y el segundo elemento es su definición.
    Realiza chequeos de compatibilidad entre Wiktionary y pattern.
    Permite ingresar una definición propia en caso de que corresponda.
    :param palabra: Palabra de la que se va a buscar definición.
    :return: None
    """
    copia = palabra
    try:
        palabra = Wiktionary(language='es').search(palabra)

        num = 0
        corte = False
        while True:
            try:
                # Las condiciones de las sentencias if contemplan "forma verbal, adjetival, etc"
                if 'sustantiv' in palabra.sections[num].title.lower():
                    if 'NN' not in parse(copia).split('/'):
                        reporte(copia, 'Distinta clasificacion')
                    break
                elif 'adjet' in palabra.sections[num].title.lower():
                    if 'JJ' not in parse(copia).split('/'):
                        reporte(copia, 'Distinta clasificacion')
                    break
                elif 'verb' in palabra.sections[num].title.lower():
                    if 'VB' not in parse(copia).split('/'):
                        reporte(copia, 'Distinta clasificacion')
                    break
                num += 1
            except IndexError:
                if ('JJ' in parse(copia).split('/')) and not corte:
                    # Busco la forma masculina de la palabra, por ejemplo FEA --> FEO
                    palabra = Wiktionary(language='es').search(predicative(copia))
                    corte = True  # Si vuelve a buscar y no encuentra la palabra, es que hay error
                else:
                    break
    except AttributeError:
        if copia.lower() not in verbs and copia.lower() not in spelling and (copia.lower() not in lexicon) and\
                (copia.upper() not in lexicon) and (copia.capitalize() not in lexicon):
                    reporte(copia, 'No reconocidas')
        else:
            tupla = definicion_propia(copia)
            return tupla
    try:
        string = palabra.sections[num].content
    except (IndexError, AttributeError):
        sg.Popup('La palabra {} no puede agregarse'.format(copia))
        return None
    pos = 1
    while True:
        if string[pos] in '123456789':
            ini = pos + 1
            while True:
                try:
                    if string[pos] in ('\n', '[', '*'):
                        fin = pos
                        break
                    pos += 1
                except IndexError:
                    fin = len(string)
                    break
            break
        pos += 1
    tupla = (palabra.sections[num].title, string[ini:fin])
    return tupla


def definicion_propia(palabra=str):
    """
    Recibe una palabra, busca el tipo de palabra en pattern y permite que el usuario ingrese una definición.
    El mismo código especifica si la palabra será considerada verbo, adjetivo o sustantivo, para evitar confusiones
    en casos de palabras ambiguas, por ejemplo colores (pueden ser sustantivos o adjetivos)
    :param palabra: string palabra
    :return: (tipo de palabra, definicion)
    """
    tipo = parse(palabra).split('/')
    if 'NN' in tipo:
        tipo = 'sustantivo'
    elif 'JJ' in tipo:
        tipo = 'adjetivo'
    else:
        tipo = 'verbo'

    layout_propia = [
        [sg.Text('Ingresar definición de {pal} ({tipo}):'.format(pal=palabra, tipo=tipo))],
        [sg.Multiline(size=(45, 10), change_submits=True, key='definicion_propia')],
        [sg.OK('Aceptar'), sg.Cancel('Cerrar')]
    ]
    ventana = sg.Window('Definiciones').Layout(layout_propia)
    while True:
        boton, datos = ventana.Read()
        if boton is 'Cerrar' or boton is None:
            def_propia = ''
            break
        elif boton is 'Aceptar':
            def_propia = datos['definicion_propia']
            break
    ventana.Close()
    return tipo, def_propia


def agregar_palabras():
    """
    Crea un diccionario a partir del archivo "palabras.json" donde chequea cada palabra que se quiere agregar,
    la agrega en caso de corresponder y luego lo sobreescribe.
    :return: None
    """
    path_palabras = '.' + os.sep + 'recursos' + os.sep + 'palabras.json'

    layout_agregar = [
        [sg.Text('Ingresá la palabra a buscar:'), sg.InputText(key='palabra')],
        [sg.OK('Agregar'), sg.Cancel('Cerrar')]
    ]

    layout = [
        [sg.Frame('Agregar Palabras', layout_agregar)]
    ]

    ventana = sg.Window('Agregar').Layout(layout)
    while True:

        try:
            arch = open(path_palabras, 'r')
            palabras = json.load(arch)
            arch.close()
        except:
            palabras = {'sustantivos': {}, 'verbos': {},
                        'adjetivos': {}}  # Manejo cualquier excepción creando un diccionario vacío

        boton, datos = ventana.Read()
        if boton is 'Cerrar' or boton is None:
            break
        else:
            palabra = datos['palabra'].lower()
            if palabra in [pal for pal in palabras['sustantivos']] or palabra in [pal for pal in palabras['adjetivos']]\
                    or palabra in [pal for pal in palabras['verbos']]:
                sg.Popup('La palabra {} ya existe en la base de datos'.format(palabra))
            else:
                definicion = buscar_definicion(palabra)
                if definicion is None:
                    pass
                else:
                    if 'sust' in definicion[0].lower():
                        palabras['sustantivos'].update({palabra: definicion[1]})
                        sg.Popup('Palabra agregada!')
                    elif 'verb' in definicion[0].lower():
                        palabras['verbos'].update({palabra: definicion[1]})
                        sg.Popup('Palabra agregada!')
                    elif 'adjet' in definicion[0].lower():
                        palabras['adjetivos'].update({palabra: definicion[1]})
                        sg.Popup('Palabra agregada!')
                    else:
                        sg.Popup('La palabra \'{}\' no se puede agregar puesto que no es Sustantivo, Verbo ni Adjetivo.'
                                 .format(datos['palabra']))
            with open(path_palabras, 'w') as arch:
                arch.write(json.dumps(palabras, indent=4))
                arch.close()

    ventana.Close()


def eliminar_palabras():
    """
    Crea un diccionario a partir de "palabras.json", chequea si la palabra que se desea eliminar existe y la elimina,
    luego sobreescribe el archivo json.
    :return: None
    """
    path_palabras = '.' + os.sep + 'recursos' + os.sep + 'palabras.json'

    layout_eliminar = [
        [sg.Text('Ingrese palabra a eliminar'), sg.InputText(do_not_clear=False, key='palabra')],
        [sg.OK('Eliminar'), sg.Cancel('Cerrar')]
    ]

    layout = [
        [sg.Frame('Eliminar', layout=layout_eliminar)]
    ]

    ventana = sg.Window('Eliminar Palabras').Layout(layout)

    while True:
        boton, datos = ventana.Read()

        if boton is 'Eliminar':
            try:
                arch = open(path_palabras, 'r')
                palabras = json.load(arch)
                arch.close()
            except:
                sg.Popup('Se ha producido un error al abrir la base de datos de palabras')
                palabras = {'sustantivos': {}, 'verbos': {}, 'adjetivos': {}}
            try:
                palabra = datos['palabra'].lower()
                if palabra in [pal.lower() for pal in palabras['sustantivos'].keys()]:
                    del palabras['sustantivos'][palabra]
                elif palabra in [pal.lower() for pal in palabras['adjetivos'].keys()]:
                    del palabras['adjetivos'][palabra]
                else:
                    del palabras['verbos'][palabra]
            except NameError:
                sg.Popup('La palabra {} no se encuentra en la base de datos'.format(datos['palabra']))
            except KeyError:
                sg.Popup('La palabra {} no se encuentra en la base de datos'.format(datos['palabra']))
            else:
                sg.Popup('La palabra {} ha sido eliminada'.format(datos['palabra']))
            with open(path_palabras, 'w') as arch:
                arch.write(json.dumps(palabras, indent=4))
                arch.close()
        elif boton is 'Cerrar' or boton is None:
            break
    ventana.Close()


def reporte(palabra,clasificacion):
    """
    Crea un diccionario a partir de "reporte.json", actualiza los valores y luego sobreescribe el archivo.
    :param palabra: string a eliminar
    :param clasificacion: corresponde a las llaves del diccionario, siendo "Distinta clasificacion" y "No reconocidas"
    :return: None
    """
    path_reporte = '.' + os.sep + 'recursos' + os.sep + 'reporte.json'

    try:
        arch = open(path_reporte, 'r')
        reporte = json.load(arch)
        arch.close()
    except:
        reporte = {'Distinta clasificacion': [], 'No reconocidas': []}
    if palabra not in reporte[clasificacion]:
        reporte[clasificacion].append(palabra)
    with open(path_reporte, 'w') as arch:
        arch.write(json.dumps(reporte, indent=4))
        arch.close()


def mostrar_reporte(tipo_titulo, tam_titulo, tipo_texto, tam_texto):
    """
    Toma el archivo "reporte.json" y crea un diccinario.
    Procesa su contenido y lo muestra en una ventana en caso de no ser vacío.
    :param tipo_titulo: string con nombre de tipografía a usar, se determina cuando se configuran todos los valores.
    :param tam_titulo: integer con el tamaño de la tipografía a usar
    :param tipo_texto: tipografía a usar mostrando contenidos.
    :param tam_texto: integer con el tamaño de la tipografía a usar
    :return: None
    """
    path_reporte = '.' + os.sep + 'recursos' + os.sep + 'reporte.json'

    try:
        with open(path_reporte, 'r') as arch:
            reporte = json.load(arch)
            arch.close()
    except:
        sg.Popup('No hay palabras cargadas para mostrar.')
        return None
    texto1 = 'Palabras que no coinciden la clasificacion de Wiktionary con pattern:'
    texto2 = ", ".join(reporte['Distinta clasificacion'])
    texto3 = 'Palabras que no aparecen en Wiktionary y no las reconoce pattern:'
    texto4 = ", ".join(reporte['No reconocidas'])

    layout = [
        [sg.Text(texto1, font=(tipo_titulo, tam_titulo))],
        [sg.Multiline(texto2, font=(tipo_texto, tam_texto), size=(50, 5), disabled=True)],
        [sg.Text('')],
        [sg.Text(texto3, font=(tipo_titulo, tam_titulo))],
        [sg.Multiline(texto4, font=(tipo_texto, tam_texto), size=(50, 5), disabled=True)],
        [sg.OK('Aceptar')]
    ]

    ventana = sg.Window('Palabras cargadas').Layout(layout)
    while True:
        boton, datos = ventana.Read()
        if boton is 'Aceptar' or boton is None:
            break
    ventana.Close()